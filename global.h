#ifndef CENTAURI_GLOBAL_H
#define CENTAURI_GLOBAL_H

#include <vector>
#include <GL/glew.h>

typedef std::vector<GLfloat> vGLfloat;
typedef std::vector<GLuint> vGLuint;

#endif //CENTAURI_GLOBAL_H
