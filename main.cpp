#include <SDL.h>

#include "common/App.h"

int main() {

    App app;

    unsigned int lastTime = 0, currentTime = 0;
    double last_tick = 0;
    bool tick_passed = false;
    while(!app.isQuit()){
        currentTime = SDL_GetTicks();
        double delta = (currentTime - lastTime) * 0.001;
        lastTime = currentTime;

        last_tick += delta;
        if (last_tick >= 0.5) {
            last_tick -= 0.5;
            tick_passed = true;
        } else {
            tick_passed = false;
        }

        app.Update(delta, tick_passed);
        app.Render(delta);
    }
    return 0;
}