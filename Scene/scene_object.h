#ifndef CENTAURI_SCENE_OBJECT_H
#define CENTAURI_SCENE_OBJECT_H

#include <GL/glew.h>

#include <glm/glm.hpp>

#include <vector>
#include <iostream>

#include "../common/log.h"
#include "../Model/generic_model.h"

struct physical_info{
    double mass = 0;

    glm::dvec3 speed = {0,0,0};
};

struct object_info{
    float scale = 1;

    glm::vec3 position = {0,0,0};

    float colatitude = 0;
    float longitude = 0;

    float rot_speed_lg = 0;
};

bool operator==(const object_info& lhs, const object_info& rhs);

object_info link_data(glm::vec3 position, float scale, float colatitude, float longitude, float rot_speed_lg);

struct material_info{
    float diffuse = 1;
    float ambient = 1;
};


struct light_info{
    glm::vec3 * light_pos = new glm::vec3(0.0);
    glm::vec3 light_color = {1.0, 1.0, 1.0};
    glm::vec3 ambient_color = {0.02, 0.02, 0.02};
};

class scene_object {
public:
    scene_object();
    scene_object(std::string m_name, object_info m_params, physical_info m_phys, material_info m_material);
    explicit scene_object(object_info m_params);

    virtual ~scene_object();

//    virtual void create_program();

    void set_light_uniforms();

    void bind_textures();

    int get_indice_size();

    virtual void change_lod(int detail) = 0;

    virtual glm::mat4 get_mMatrix(float delta_t) = 0;

    virtual void Draw(glm::mat4 View, glm::mat4 Proj, float delta_t) = 0;

    object_info * getParams() const;
    physical_info * getPhys();

    void setParams(object_info * info);

    bool isVisible() const;

    void setVisible(bool m_visible);

    static std::vector<light_info *> lights;

    const std::string &getName() const;

protected:
    const std::vector<GLuint> * indices_array;
    const std::vector<GLfloat> * vertices_array;
    const std::vector<GLfloat> * uv_array;
    const std::vector<GLfloat> * normals_array;

    GLuint VAO = 0;

    GLuint indices_buffer = 0;
    GLuint vertices_buffer = 0;
    GLuint uv_buffer = 0;
    GLuint normals_buffer = 0;

    GLuint program_l = 0;

//    std::vector<std::string> uniform_name;
//    std::vector<GLuint> uniformID;

    std::vector<std::vector<GLuint>> textures_info;

    generic_model * model;

    object_info * params;
    physical_info phys;
    material_info material;

    bool visible = true;

    std::string name = "";

};

#endif //CENTAURI_SCENE_OBJECT_H