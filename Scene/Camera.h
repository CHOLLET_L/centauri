#ifndef CENTAURI_CAMERA_H
#define CENTAURI_CAMERA_H

#include <glm/glm.hpp>

#include "Animation.h"

class App;
class Camera;
#include "../common/App.h"

class Camera {
public:
    explicit Camera(glm::vec3* origin);
    ~Camera() = default;
    void set_window_size(int, int);

    glm::mat4 get_viewMatrix();
    glm::mat4 get_projMatrix();
    void update_pos(double delta_t);

    void set_mouse_mvt(int x, int y);
    void set_mvt(int front_m, int right_m, int up_m);

    void set_speed(float s);

    void set_animation(glm::vec3*, float size);

    void setRelOrigin(glm::vec3 *relOrigin);
    glm::vec3 *getRelOrigin() const;

    glm::vec3 get_pos() const;
    bool is_bind() const;

private:
    glm::vec3 pos = glm::vec3(0, 0, 5);

    glm::vec3* rel_origin = {};


    glm::vec3 up = glm::vec3( 0, 1, 0 );
    glm::vec3 dir = glm::vec3( 0, 0, 0 );

    float horizontalAngle = - M_PI / 2.f;
    float verticalAngle = M_PI / 2.f;

    float speed = 1.0f;
    float mouseSpeed = 0.0015f;


    glm::mat4 vMat{};
    glm::mat4 pMat{};

    int mouse_xpos = 0;
    int mouse_ypos = 0;

    glm::vec3 mvt = {0, 0, 0};

    void update_Mats();

    bool in_animation = false;

    Animation anim;

    float w_width = 0;
    float w_height = 0;
};


#endif //CENTAURI_CAMERA_H
