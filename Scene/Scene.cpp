#include "../common/log.h"

#include "../common/RK4.h"

#include "Scene.h"

Scene::Scene() {
    config.load_from_file("../Scene/Scene_Configuration/Solar_System.xml");
    load_from_data();

    for (auto & object : objects) {
        object->getParams()->scale *= 10;
    }
//    dprint(objects[1]->getParams()->position[0], 0);
//    dprint(objects[1]->getParams()->position[1], 0);
//    dprint(objects[1]->getParams()->position[2], 0);
//    exit(0);

    //// Initialize Camera
    origin = new glm::vec3(0, 0, 0);
    camera = new Camera(origin);
}

const std::vector<scene_object *> &Scene::getObjects() const {
    return objects;
}

void Scene::Update_scene(double delta) {
    update_position(objects, delta);
    camera->set_speed(compute_camera_speed());
    camera->update_pos(delta);
}

void Scene::Render(double delta) {
    glm::mat4 View = camera->get_viewMatrix();
    glm::mat4 Projection = camera->get_projMatrix();

    for (scene_object* object : objects){
        if (object->isVisible()){
            object->Draw(View, Projection, (float) delta);
        }
    }
}

int Scene::get_closest_obj(glm::vec3 x) {
    float min = glm::length(x - objects[0]->getParams()->position);
    int id = 0;
    for(int i = 1; i < objects.size(); ++i){
        object_info a{};
        if (*objects[i]->getParams() == a) continue; // if object is a non-valable scene_object (Skybox etc)

        float dist = glm::length(x - objects[i]->getParams()->position);
        if (dist < min) {
            min = dist;
            id = i;
        }
    }
    return id;
}

int Scene::get_id_by_name(const std::string& name) {
    for(int id = 0; id < objects.size(); ++id){
        if (name == objects[id]->getName()) return id;
    }
    dprint("Unable to find object with name: " + name, 1);
    return 0;
}

float Scene::compute_camera_speed() {
    float pos_coef = 1;
    if (camera->is_bind()){
        int id = get_closest_obj(*camera->getRelOrigin());

        glm::vec3 dist = camera->get_pos() - objects[id]->getParams()->position;
        pos_coef = glm::length(dist) * objects[id]->getParams()->scale * 10000;
    }

    float speed = pos_coef
            * camera_speed_coef;
    return speed;
}

void Scene::set_camera_speed_coef(float coef){
    camera_speed_coef = coef;
}

Scene::Scene(const Scene &) {
    dprint("Unexpected comportment: " + ptr_to_str(this), 2);
}

Scene::~Scene() {
    delete camera;
    delete origin;

    int s = objects.size();
    for (int i = 0; i<s ; ++i){
        delete objects[i];
    }
}

Scene & Scene::operator=(const Scene &) {
    dprint("Unexpected comportment: " + ptr_to_str(this), 2);
    return *this;
}