#include <utility>

#include "../Model/cube_model.h"
#include "../Shader/Shader_compiler.h"
#include "../Textures/Texture_loader.h"

#include "Skybox.h"

bool Skybox::program_created = false;
GLuint Skybox::programID = 0;

Skybox::Skybox(std::string path) : texture_directory_path(std::move(path)){
    initialize_object();
//    dprint(params.colatitude, 0);
}

void Skybox::initialize_object() {
    if (!program_created) {
        create_program();
        program_created = true;
    }
    program_l = Skybox::programID;


    model = new cube_model;

    vertices_array = model->get_vertices();

    glBindVertexArray(VAO);

    fill_buffers();

    getUniformsLocation();
    initialize_textures();

    glBindVertexArray(0);
}

void Skybox::getUniformsLocation(){
    vpID = glGetUniformLocation(program_l, "VP");
    cube_sampler_textureID = glGetUniformLocation(program_l, "skybox_cube");
}

void Skybox::create_program() {
    Skybox::programID = program_creator(shader_creator(GL_VERTEX_SHADER, "../Shader/skybox_vertex.glsl"),
                                          shader_creator(GL_FRAGMENT_SHADER, "../Shader/skybox_fragment.glsl"));
    dprint("Skybox Compiled", 0);
}

void Skybox::setUniformsValue(const glm::mat4& vp) {
    glUniformMatrix4fv(vpID, 1, GL_FALSE, &vp[0][0]);
    glUniform1i(cube_sampler_textureID, 0);
}

void Skybox::fill_buffers() {
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertices_buffer);
    glBufferData(GL_ARRAY_BUFFER, vertices_array->size() * sizeof(GLfloat), vertices_array->data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
}

void Skybox::initialize_textures() {
    cube_textureID = Load_cube_texture_DDS("../Textures/skybox/2k_star/");
}

void Skybox::bind_cube_texture() {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cube_textureID);
}

void Skybox::Draw(glm::mat4 View, glm::mat4 Proj, float delta_t) {
    glBindVertexArray(VAO);

    program_l = Skybox::programID;
    glUseProgram(program_l);

    glDepthFunc(GL_LEQUAL);

    bind_cube_texture();

    glm::mat4 view_without_translation = glm::mat4(glm::mat3(View)); // Translation parameters are on the last column
    setUniformsValue(Proj * view_without_translation);

    glDrawArrays(GL_TRIANGLES, 0, 36);

    glDepthFunc(GL_LESS);

    glBindVertexArray(0);
}

void Skybox::change_lod(int) {
    dprint("Unexpected comportment", 2);
}

glm::mat4 Skybox::get_mMatrix(float) {
    dprint("Unexpected comportment", 2);
    return glm::mat4(1.0f);
}

Skybox::~Skybox() {
    delete params;
}
