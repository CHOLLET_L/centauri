#ifndef CENTAURI_SCENE_H
#define CENTAURI_SCENE_H

class Scene;
#include "Camera.h"
#include "../common/Data_Structure.h"
#include "scene_object.h"


class Scene {
public:
    Scene();
    Scene(const Scene &);
    ~Scene();
    Scene &operator=(const Scene &);

    void Update_scene(double delta);
    void Render(double delta);

    // XML_Scene.cpp
    void load_from_data();
    void set_to_data();

    int get_closest_obj(glm::vec3 x);
    int get_id_by_name(const std::string&);

    const std::vector<scene_object *> &getObjects() const;
    void set_camera_speed_coef(float);

    Camera* camera{};
    glm::vec3 *origin;

private:

    float compute_camera_speed();

    std::vector<scene_object*> objects;
    Data_Structure config;

    float camera_speed_coef = 0;
};


#endif //CENTAURI_SCENE_H
