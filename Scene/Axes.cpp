#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

#include "../Shader/Shader_compiler.h"
#include "../Model/axes_model.h"

#include "Axes.h"

bool Axes::program_created = false;
GLuint Axes::programID = 0;

Axes::Axes() : scene_object() {
    initialize_object();
}

Axes::Axes(object_info info) : scene_object(info) {
    initialize_object();
}

Axes::Axes(const scene_object& s_o) : scene_object() {
    linked = true;

//// TODO : fix the crappy delete of params defined in scene_object default constructor
    delete params;

    params = s_o.getParams();
    initialize_object();
}

void Axes::initialize_object() {
    if (!program_created) {
        create_program();
        program_created = true;
    }
    program_l = Axes::programID;


    model = new axes_model;

    vertices_array = model->get_vertices();
    uv_array = model->get_uv();
    indices_array = model->get_indices();
//    const std::vector<GLfloat>* test = model->get_uv();
//    for (GLfloat elem : *test){
//        std::cout << elem << std::endl;
//    }

    glBindVertexArray(VAO);

    fill_buffers();

    getUniformsLocation();

    glBindVertexArray(0);
}

void Axes::getUniformsLocation(){
    mvpID = glGetUniformLocation(program_l, "MVP");
}

void Axes::create_program() {
    Axes::programID = program_creator(shader_creator(GL_VERTEX_SHADER, "../Shader/axes_vertex.glsl"),
                                      shader_creator(GL_FRAGMENT_SHADER, "../Shader/axes_fragment.glsl"));
    dprint("Axes Compiled", 0);
}

void Axes::setUniformsValue(const glm::mat4& mvp) {
    glUniformMatrix4fv(mvpID, 1, GL_FALSE, &mvp[0][0]);
}

void Axes::fill_buffers() {
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertices_buffer);
    glBufferData(GL_ARRAY_BUFFER, vertices_array->size() * sizeof(GLfloat), vertices_array->data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, uv_buffer); // Colors in this case
    glBufferData(GL_ARRAY_BUFFER, uv_array->size() * sizeof(GLfloat), uv_array->data(), GL_STATIC_DRAW);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr); // size of 3, colors
}

void Axes::Draw(glm::mat4 View, glm::mat4 Proj, float delta_t) {
    glBindVertexArray(VAO);

    program_l = Axes::programID;
    glUseProgram(program_l);

    setUniformsValue(Proj * View * get_mMatrix(delta_t));

    glDrawArrays(GL_LINES, 0, 6);

    glBindVertexArray(0);
}

glm::mat4 Axes::get_mMatrix(float delta) {
    double axes_scale = params->scale * 1.5;

    glm::quat rot(glm::vec3(-params->colatitude, 0, 0));
    glm::quat rot2(glm::vec3(0, params->longitude, 0));


    glm::mat4 transform_matrix = glm::mat4(1.0f);
    transform_matrix *= glm::translate(glm::mat4(1.0f), params->position);
    transform_matrix *= glm::toMat4(rot*rot2);
    transform_matrix *= glm::scale(glm::mat4(1.0f), glm::vec3(axes_scale, axes_scale, axes_scale));


    return transform_matrix;
}

void Axes::change_lod(int) {
    dprint("Unexpected comportment", 2);
}

Axes::~Axes() {
    if (!linked){
//        dprint(ptr_to_str(this), 0);
        delete params;
    }
}
