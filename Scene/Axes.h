#ifndef CENTAURI_AXES_H
#define CENTAURI_AXES_H

#include "scene_object.h"

class Axes :  public scene_object{
public:
    Axes();
    explicit Axes(object_info info);
    explicit Axes(const scene_object& s_o);

    ~Axes() override;

    static void create_program();


    void Draw(glm::mat4 View, glm::mat4 Proj, float delta_t) override;

private:
    void initialize_object();

    bool linked = false;

    static bool program_created;
    static GLuint programID;

    GLuint mvpID=0;

    void getUniformsLocation();
    void setUniformsValue(const glm::mat4& mvp);
    void fill_buffers();

    void change_lod(int) override;
    glm::mat4 get_mMatrix(float) override;
};


#endif //CENTAURI_AXES_H
