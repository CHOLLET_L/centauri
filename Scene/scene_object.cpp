#include "scene_object.h"

#include <utility>

std::vector<light_info *> scene_object::lights;

scene_object::scene_object() {
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    glGenBuffers(1, &vertices_buffer);
    glGenBuffers(1, &uv_buffer);
    glGenBuffers(1, &normals_buffer);
    glGenBuffers(1, &indices_buffer);

    params = new object_info;
}

scene_object::scene_object(object_info m_params) {
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    glGenBuffers(1, &vertices_buffer);
    glGenBuffers(1, &uv_buffer);
    glGenBuffers(1, &normals_buffer);
    glGenBuffers(1, &indices_buffer);

    params = new object_info(m_params);
}

scene_object::scene_object(std::string m_name, object_info m_params, physical_info m_phys, material_info m_material) {
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    glGenBuffers(1, &vertices_buffer);
    glGenBuffers(1, &uv_buffer);
    glGenBuffers(1, &normals_buffer);
    glGenBuffers(1, &indices_buffer);

    params = new object_info(m_params);
    phys = m_phys;
    material = m_material;

    name = std::move(m_name);
}

scene_object::~scene_object() {
    delete model;

    int s = scene_object::lights.size();
    for (int i = 0; i < s; i++){
        delete scene_object::lights[i];
    }
    scene_object::lights.clear();
}

void scene_object::bind_textures() {
    for(int i = 0; i<textures_info.size(); i++){
//        dprint("Texture : " + std::to_string(i),0);
        glActiveTexture(GL_TEXTURE0+i);
        glBindTexture(GL_TEXTURE_2D, textures_info[i][0]);
        glUniform1i(textures_info[i][1], i);
    }
}

int scene_object::get_indice_size(){
//    dprint("Vert:" + ptr_to_str(vertices_array),0);
//    dprint("Uv:" + ptr_to_str(uv_array),0);
//    dprint("Indices:" + ptr_to_str(indices_array),0);
    return indices_array->size();
}

object_info * scene_object::getParams() const {
    return params;
}

void scene_object::setParams(object_info * info) {
    params = info;
}

physical_info * scene_object::getPhys() {
    return &phys;
}

void scene_object::set_light_uniforms() {
    for (GLuint i = 0; i < scene_object::lights.size(); i++)
    {
        std::string number = std::to_string(i);
        glUniform3f(glGetUniformLocation(program_l, ("lights[" + number + "].light_pos").c_str()), lights[i]->light_pos->x, lights[i]->light_pos->y, lights[i]->light_pos->z);

        glUniform3f(glGetUniformLocation(program_l, ("lights[" + number + "].light_color").c_str()), lights[i]->light_color.x, lights[i]->light_color.y, lights[i]->light_color.z);
        glUniform3f(glGetUniformLocation(program_l, ("lights[" + number + "].ambient_color").c_str()), lights[i]->ambient_color.x, lights[i]->ambient_color.y, lights[i]->ambient_color.z);
    }

    glUniform1f(glGetUniformLocation(program_l, "material.ambient"), material.ambient);
    glUniform1f(glGetUniformLocation(program_l, "material.diffuse"), material.diffuse);
}

bool scene_object::isVisible() const {
    return visible;
}

void scene_object::setVisible(bool m_visible) {
    scene_object::visible = m_visible;
}

void scene_object::change_lod(int detail) {
    dprint("Virtual Parent method called by " + ptr_to_str(this) ,1);
}


glm::mat4 scene_object::get_mMatrix(float now) {
    dprint("Virtual Parent method called by " + ptr_to_str(this) ,1);return glm::mat4(1.0f);
}

const std::string &scene_object::getName() const {
    return name;
}


bool operator==(const object_info& lhs, const object_info& rhs)
{
    bool a = lhs.scale == rhs.scale;
    bool b = glm::all(glm::equal(lhs.position, rhs.position));
    bool c = lhs.colatitude == rhs.colatitude;
    bool d = lhs.longitude == rhs.longitude;
    bool e = lhs.rot_speed_lg == rhs.rot_speed_lg;
    return a && b && c && d && e;
}

object_info link_data(glm::vec3 position, float scale, float colatitude, float longitude, float rot_speed_lg){
    object_info inf;
    inf.position = position;
    inf.scale = scale;
    inf.colatitude = colatitude;
    inf.longitude = longitude;
    inf.rot_speed_lg = rot_speed_lg;
    return inf;
}
