//#include <cmath>
#include <glm/glm.hpp>

#include "Animation.h"
#include "../common/log.h"


float gauss_speed(double x){
//    return (float) (.5*erf((x-.5)*4) + .5);

//    return (float) 1.f / (1.f + (float) exp(-20*(x-.5)));

    // Polynomial form:
    // ax^6 + bx^5 + cx^4 + dx^3 + ex^2
    // a = 0.00000001159
    // b = 5.5555555361
    // c = −13.8888888786
    // d = 9.11111113355
    // e = 0.222222222288
    //// Conditions: f'(1)=0 & f'(0)=0 & f(0)=0 & f(1)=1 & f(0.1)=0.01 & f(0.9)=0.99

     float a = 0.00000001159;
     float b = 5.5555555361;
     float c = -13.8888888786;
     float d = 9.11111113355;
     float e = 0.222222222288;
     
     return (float) (a*pow(x, 6) + b*pow(x, 5) + c*pow(x, 4) + d*pow(x, 3) + e*pow(x, 2));
}

float linear_rep(double x){
    auto ret = (float) (x * (1.f / 0.3));
    if (ret > 1) ret = 1;
    return ret;
}

// Axes follow OpenGL Convention - index to the top

glm::vec3 cart_to_sphe(const glm::vec3 &cart){
    auto r = (float) std::sqrt(cart[0]*cart[0] + cart[1]*cart[1] + cart[2]*cart[2]);
    glm::vec3 sphe = {r, atan2(cart[2], cart[0]), std::acos(cart[1] / r)};
    return sphe;
}

glm::vec3 sphe_to_cart(const glm::vec3 &sphe){
    return glm::vec3(
            sphe[0] * std::sin(sphe[2]) * std::cos(sphe[1]),
            sphe[0] * std::cos(sphe[2]),
            sphe[0] * std::sin(sphe[2]) * std::sin(sphe[1]));
}

void Animation::new_anim(glm::vec3 actual, glm::vec3 dest, glm::vec3 dir) {
    start_position = actual;
    dest_position = dest;
    start_sphe_direction = cart_to_sphe(glm::normalize(dir));
    dest_sphe_direction = cart_to_sphe(-glm::normalize(dest));
    total = 0;
}

glm::vec3 Animation::get_pos() {
    return position;
}
glm::vec3 Animation::get_dir() {
    return direction;
}

bool Animation::update_pos(double delta) {
    if (total <= 1){

        position = start_position + gauss_speed(total) * (dest_position - start_position);
        direction = sphe_to_cart(start_sphe_direction + linear_rep(total) * (dest_sphe_direction - start_sphe_direction));
        total += delta / 2.5;
        return true;
    } else {
        position = dest_position;
        direction = sphe_to_cart(dest_sphe_direction);
        return false;
    }
}
