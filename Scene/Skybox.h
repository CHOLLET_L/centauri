#ifndef CENTAURI_SKYBOX_H
#define CENTAURI_SKYBOX_H

#include "scene_object.h"

class Skybox : public scene_object {
public:
    Skybox(std::string path);

    ~Skybox() override;

    void Draw(glm::mat4 View, glm::mat4 Proj, float delta_t) override;

private:
    static void create_program();

    void initialize_object();
    void initialize_textures();

    static bool program_created;
    static GLuint programID;

    std::string texture_directory_path;

    GLuint vpID=0;
    GLuint cube_textureID=0;
    GLuint cube_sampler_textureID=0;

    void getUniformsLocation();
    void setUniformsValue(const glm::mat4& vp);
    void fill_buffers();
    void bind_cube_texture();

    void change_lod(int) override;
    glm::mat4 get_mMatrix(float) override;
};


#endif //CENTAURI_SKYBOX_H
