#ifndef CENTAURI_ANIMATION_H
#define CENTAURI_ANIMATION_H

#include <glm/glm.hpp>

class Animation {
public:
    void new_anim(glm::vec3 actual, glm::vec3 dest, glm::vec3 dir);

    glm::vec3 get_pos();
    glm::vec3 get_dir();

    bool update_pos(double delta);
private:
    glm::vec3  start_position;
    glm::vec3  dest_position;
    glm::vec3  position = {0, 0, 0};

    glm::vec3  start_sphe_direction = {0, 0, 0};
    glm::vec3  dest_sphe_direction = {0, 0, 0};
    glm::vec3  direction = {0, 0, 0};

    double total = 0;
};

glm::vec3 cart_to_sphe(const glm::vec3 &cart);
glm::vec3 sphe_to_cart(const glm::vec3 &sphe);

#endif //CENTAURI_ANIMATION_H
