#ifndef CENTAURI_SPHERE_H
#define CENTAURI_SPHERE_H

#include "scene_object.h"

class Sphere : public scene_object{
public:
    Sphere(std::string m_name, object_info inf, std::string texture_path, material_info m_material, physical_info m_phys);

    Sphere(const Sphere &S);
    Sphere& operator=(const Sphere& S);

    ~Sphere() override;

    void change_lod(int detail) override;
    glm::mat4 get_mMatrix(float delta_t) override;

    void Draw(glm::mat4 View, glm::mat4 Proj, float delta_t) override;

private:
    static void create_program();
    static bool program_created;

    static GLuint programID;

    float angle_p = 0; // Angle of the planet relative to a fixed star

    std::string texture_path = "";

    void initialize_textures();

    void getUniformsLocation();
    void setUniformsValue(const glm::mat4& m, const glm::mat4& vp, const glm::mat4& inv_m);
    void fill_buffers();

    GLuint mID;
    GLuint vpID;
    GLuint inv_mID;
};

#endif //CENTAURI_SPHERE_H
