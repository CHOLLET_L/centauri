#include <utility>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

#include "../Model/Icosahedron.h"
#include "../Textures/Texture_loader.h"
#include "../Shader/Shader_compiler.h"

#include "Sphere.h"

#ifdef DEBUG_SHADER
#define GEOMETRY_SHADER shader_creator(GL_GEOMETRY_SHADER, "../Shader/debug_geometry.glsl")
#else
#define GEOMETRY_SHADER 0
#endif

bool Sphere::program_created = false;
GLuint Sphere::programID = 0;


Sphere::Sphere(std::string m_name, object_info obj_inf = {1, {0, 0, 0}, 0, 0, 0}, std::string texture_path = "", material_info m_material = {0, 0}, physical_info m_phys = {0.0, {0.0, 0.0, 0.0}})
            : scene_object(std::move(m_name), obj_inf, m_phys, m_material),
        texture_path(std::move(texture_path)) {

    model = new Icosahedron;

    indices_array = model->get_indices();
    vertices_array = model->get_vertices();
    uv_array = model->get_uv();
    normals_array = model->get_normals();

    if (!program_created){
        create_program();
        program_created = true;
    }
    program_l = Sphere::programID;

    glBindVertexArray(VAO);

    fill_buffers();

    getUniformsLocation();
    initialize_textures();

    glBindVertexArray(0);
}

void Sphere::change_lod(int detail) {
//    dprint(detail, 0);

    model->create_lod(detail);

    glBindVertexArray(VAO);
    fill_buffers();
    glBindVertexArray(0);
}


void Sphere::getUniformsLocation(){
    mID = glGetUniformLocation(program_l, "M");
    vpID = glGetUniformLocation(program_l, "VP");
    inv_mID = glGetUniformLocation(program_l, "inv_M");
}

void Sphere::initialize_textures() {
    std::vector<GLuint> texture_0;

    texture_0.push_back(Load_texture_DDS(texture_path));
    texture_0.push_back(glGetUniformLocation(program_l, "texture_sampler"));

    textures_info.push_back(texture_0);
}

void Sphere::setUniformsValue(const glm::mat4& m, const glm::mat4& vp, const glm::mat4& inv_m) {
    glUniformMatrix4fv(mID, 1, GL_FALSE, &m[0][0]);
    glUniformMatrix4fv(vpID, 1, GL_FALSE, &vp[0][0]);
    glUniformMatrix4fv(inv_mID, 1, GL_FALSE, &inv_m[0][0]);
}

void Sphere::fill_buffers() {
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertices_buffer);
    glBufferData(GL_ARRAY_BUFFER, vertices_array->size() * sizeof(GLfloat), vertices_array->data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, uv_buffer);
    glBufferData(GL_ARRAY_BUFFER, uv_array->size() * sizeof(GLfloat), uv_array->data(), GL_STATIC_DRAW);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, normals_buffer);
    glBufferData(GL_ARRAY_BUFFER, normals_array->size() * sizeof(GLfloat), normals_array->data(), GL_STATIC_DRAW);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices_buffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_array->size() * sizeof(GLfloat), indices_array->data(), GL_STATIC_DRAW);
}

void Sphere::create_program() {
    Sphere::programID = program_creator(shader_creator(GL_VERTEX_SHADER, "../Shader/sphere_vertex.glsl"),
                              shader_creator(GL_FRAGMENT_SHADER, "../Shader/sphere_fragment.glsl"),
                              GEOMETRY_SHADER);
    dprint("Sphere Compiled", 0);
}


glm::mat4 Sphere::get_mMatrix(float delta) {
    params->longitude += params->rot_speed_lg * delta;

//    params->position = glm::rotate((float) M_PI/30 * delta ,glm::vec3(0, 1, 0)) *
//            glm::vec4(params->position[0], params->position[1], params->position[2], 0);

    glm::quat rot(glm::vec3(-params->colatitude, 0, 0));
    glm::quat rot2(glm::vec3(0, params->longitude, 0));


    glm::mat4 transform_matrix = glm::mat4(1.0f);
    transform_matrix *= glm::translate(glm::mat4(1.0f), params->position);
    transform_matrix *= glm::toMat4(rot*rot2);
    transform_matrix *= glm::scale(glm::mat4(1.0f), glm::vec3(params->scale, params->scale, params->scale));

    return transform_matrix;
}

void Sphere::Draw(glm::mat4 View, glm::mat4 Proj, float delta_t) {
    glBindVertexArray(VAO);

    program_l = Sphere::programID;
    glUseProgram(program_l);

    glm::mat4 Model = get_mMatrix(delta_t);
    setUniformsValue(Model, Proj * View, glm::transpose(glm::inverse(Model)));

    bind_textures();

    set_light_uniforms();

    glDrawElements(
            GL_TRIANGLES,
            get_indice_size(),
            GL_UNSIGNED_INT,
            nullptr);

    glBindVertexArray(0);
}



Sphere& Sphere::operator=(const Sphere& S) {
    dprint("Unexpected comportment", 2);
    return *this;
}

Sphere::Sphere(const Sphere &S) : scene_object(S) {
    dprint("Unexpected comportment", 2);
}

Sphere::~Sphere() {
    delete params;
}
