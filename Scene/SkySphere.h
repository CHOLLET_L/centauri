#ifndef CENTAURI_SKYSPHERE_H
#define CENTAURI_SKYSPHERE_H

#include "scene_object.h"

class SkySphere : public scene_object {
public:
    explicit SkySphere(std::string m_name);

    ~SkySphere() override;

    void Draw(glm::mat4 View, glm::mat4 Proj, float delta_t) override;

private:
    static void create_program();

    void initialize_textures();

    static bool program_created;
    static GLuint programID;

    std::string texture_path;

    GLuint vpID=0;

    void getUniformsLocation();
    void setUniformsValue(const glm::mat4& vp);
    void fill_buffers();

    void change_lod(int) override;
    glm::mat4 get_mMatrix(float) override;

};

#endif //CENTAURI_SKYSPHERE_H
