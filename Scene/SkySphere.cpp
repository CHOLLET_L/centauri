#include "SkySphere.h"

#include <utility>

#include "../Model/cube_model.h"
#include "../Model/Icosahedron.h"
#include "../Shader/Shader_compiler.h"
#include "../Textures/Texture_loader.h"


bool SkySphere::program_created = false;
GLuint SkySphere::programID = 0;

SkySphere::SkySphere(std::string texture_path)
        : texture_path(std::move(texture_path)) {

    model = new Icosahedron;

    indices_array = model->get_indices();
    vertices_array = model->get_vertices();
    uv_array = model->get_uv();

    if (!program_created){
        create_program();
        program_created = true;
    }
    program_l = SkySphere::programID;

    glBindVertexArray(VAO);

    fill_buffers();

    getUniformsLocation();
    initialize_textures();

    glBindVertexArray(0);
}

void SkySphere::change_lod(int detail) {
    model->create_lod(detail);

    glBindVertexArray(VAO);
    fill_buffers();
    glBindVertexArray(0);
}


void SkySphere::getUniformsLocation(){
    vpID = glGetUniformLocation(program_l, "VP");
}

void SkySphere::initialize_textures() {
    std::vector<GLuint> texture_0;

    texture_0.push_back(Load_texture_DDS(texture_path));
    texture_0.push_back(glGetUniformLocation(program_l, "texture_sampler"));

    textures_info.push_back(texture_0);
}

void SkySphere::setUniformsValue(const glm::mat4& vp) {
    glUniformMatrix4fv(vpID, 1, GL_FALSE, &vp[0][0]);
}

void SkySphere::fill_buffers() {
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertices_buffer);
    glBufferData(GL_ARRAY_BUFFER, vertices_array->size() * sizeof(GLfloat), vertices_array->data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, uv_buffer);
    glBufferData(GL_ARRAY_BUFFER, uv_array->size() * sizeof(GLfloat), uv_array->data(), GL_STATIC_DRAW);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices_buffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_array->size() * sizeof(GLfloat), indices_array->data(), GL_STATIC_DRAW);
}

void SkySphere::create_program() {
    SkySphere::programID = program_creator(shader_creator(GL_VERTEX_SHADER, "../Shader/skysphere_vertex.glsl"),
                                        shader_creator(GL_FRAGMENT_SHADER, "../Shader/skysphere_fragment.glsl"));
    dprint("SkySphere Compiled", 0);
}


glm::mat4 SkySphere::get_mMatrix(float) {
    dprint("Unexpected comportment", 2);
    return glm::mat4(1.0f);
}

void SkySphere::Draw(glm::mat4 View, glm::mat4 Proj, float delta_t) {
    glBindVertexArray(VAO);

    program_l = SkySphere::programID;
    glUseProgram(program_l);

    glDepthFunc(GL_LEQUAL);
    glDisable(GL_CULL_FACE);

    glm::mat4 view_without_translation = glm::mat4(glm::mat3(View)); // Translation parameters are on the last column
    setUniformsValue(Proj * view_without_translation);
//    setUniformsValue(Proj * View);

    bind_textures();

    glDrawElements(
            GL_TRIANGLES,
            get_indice_size(),
            GL_UNSIGNED_INT,
            nullptr);

    glDepthFunc(GL_LESS);
    glEnable(GL_CULL_FACE);

    glBindVertexArray(0);
}

SkySphere::~SkySphere() {
    delete params;
}
