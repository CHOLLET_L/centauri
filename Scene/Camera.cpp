#include <cmath>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Camera.h"
#include "../common/log.h"

Camera::Camera(glm::vec3* origin) {
    rel_origin = origin;
}

glm::mat4 Camera::get_projMatrix() {return pMat;}
glm::mat4 Camera::get_viewMatrix() {return vMat;}

void Camera::update_pos(double delta_t) {
    if (in_animation){
        if(!anim.update_pos(delta_t)) {
            in_animation = false;
            App::reset_mouse_pos();

            pos = anim.get_pos();
            dir = anim.get_dir();

            glm::vec3 angles = cart_to_sphe(dir);

            horizontalAngle = angles[1];
            verticalAngle = angles[2];

        } else {
            pos = anim.get_pos();
            dir = anim.get_dir();
        }

    } else {
        horizontalAngle -= mouseSpeed * (float)mouse_xpos;
        verticalAngle -= mouseSpeed * (float)mouse_ypos;

        dir = sphe_to_cart(glm::vec3(1, horizontalAngle, verticalAngle));

        glm::vec3 right = glm::vec3(
                std::cos(horizontalAngle + M_PI/2.0f),
                0,
                std::sin(horizontalAngle + M_PI/2.0f)
        );

        up = glm::cross(right, dir);

        pos += (mvt[0] * dir + mvt[1] * right + mvt[2] * up) * (float) delta_t * speed;
    }

    mouse_xpos = 0; mouse_ypos = 0;
    mvt[0] = 0; mvt[1] = 0; mvt[2] = 0;

    update_Mats();
}

void Camera::update_Mats() {
    pMat = glm::perspective(glm::radians((float)45.0), w_width / w_height, 0.00001f, 100.0f);

    vMat = glm::lookAt(
            pos + *rel_origin,
            pos + *rel_origin + dir,
            up
    );
}

void Camera::set_mouse_mvt(int x, int y) {
    mouse_xpos = x;
    mouse_ypos = y;
}

void Camera::set_mvt(int front_m, int right_m, int up_m) {
    mvt = {front_m, right_m, up_m};
}

void Camera::set_speed(float s) {
    speed = s;
}

void Camera::set_animation(glm::vec3* dest, float size) {
    setRelOrigin(dest);
    in_animation = true;
    anim.new_anim(pos,  glm::normalize(pos) * size * 10.f, dir);
}


void Camera::setRelOrigin(glm::vec3 *relOrigin) {
    if (rel_origin != nullptr){
        pos += *rel_origin;
    }
    pos -= *relOrigin;
    rel_origin = relOrigin;
}

void Camera::set_window_size(int width, int height) {
    w_width = (float) width;
    w_height = (float) height;
}

glm::vec3 Camera::get_pos() const {
    return pos + *rel_origin;
}

bool Camera::is_bind() const {
    return ! glm::all(glm::equal(*rel_origin, glm::vec3(0, 0, 0)));
}

glm::vec3 *Camera::getRelOrigin() const {
    return rel_origin;
}
