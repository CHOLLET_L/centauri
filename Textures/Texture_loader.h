#ifndef CENTAURI_TEXTURE_LOADER_H
#define CENTAURI_TEXTURE_LOADER_H

#include <iostream>

#define 	FOURCC_DXT1   0x31545844
#define 	FOURCC_DXT3   0x33545844
#define 	FOURCC_DXT5   0x35545844

GLuint Load_texture_DDS(const std::string& imagepath);
GLuint Load_cube_texture_DDS(const std::string& directory_path);

#endif //CENTAURI_TEXTURE_LOADER_H
