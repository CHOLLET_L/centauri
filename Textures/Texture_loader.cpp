#include <GL/glew.h>

#include <fstream>
#include <cstring>
#include <vector>

#include "../common/log.h"
#include "Texture_loader.h"

// Useful https://gist.github.com/tilkinsc/13191c0c1e5d6b25fbe79bbd2288a673

GLuint Load_texture_DDS(const std::string& imagepath){
    std::ifstream ddsFile (imagepath, std::ios::in | std::ios::binary);

    if (!ddsFile.is_open()) {
        dprint("Unable to find file : " + imagepath, 2);
        ddsFile.close();
        return -1;
    }

    char format_code[4];
    ddsFile.read(format_code, 4);
    if (strncmp(format_code, "DDS ", 4) != 0){
        dprint("Loaded file is not a DDS file", 2);
        ddsFile.close();
        return -1;
    }

    char header[124];
    ddsFile.read(header, 124);

    unsigned int height = *(unsigned int*)&(header[8]);
    unsigned int width = *(unsigned int*)&(header[12]);
    unsigned int linearSize = *(unsigned int*)&(header[16]);
    unsigned int mipMapCount = *(unsigned int*)&(header[24]);
    unsigned int fourCC = *(unsigned int*)&(header[80]);

    unsigned int buffsize = mipMapCount > 1 ? linearSize*2: linearSize;

    auto buffer = (char *)malloc(buffsize * sizeof(char));
    ddsFile.read(buffer, buffsize);
    ddsFile.close();

    unsigned int format;
    switch(fourCC)
    {
        case FOURCC_DXT1:
            format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
            break;
        case FOURCC_DXT3:
            format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
            break;
        case FOURCC_DXT5:
            format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
            break;
        default:
            free(buffer);
            return 0;
    }

    GLuint textureID;
    glGenTextures(1, &textureID);

    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, mipMapCount-1); // opengl likes array length of mipmaps
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); // don't forget to enable mipmaping
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    unsigned int blockSize = (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16;
    unsigned int offset = 0;

    for (unsigned int level = 0; level < mipMapCount; ++level)
    {
        if(width == 0 || height == 0) { // discard any odd mipmaps 0x1 0x2 resolutions
            mipMapCount--;
            continue;
        }
        unsigned int size = ((width+3)/4)*((height+3)/4)*blockSize;
        glCompressedTexImage2D(GL_TEXTURE_2D, level, format, width, height,
                               0, size, buffer + offset);

        offset += size;
        width  /= 2;
        height /= 2;
    }
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, mipMapCount-1);


    glBindTexture(GL_TEXTURE_2D, 0);


    free(buffer);

    return textureID;
}

GLuint Load_cube_texture_DDS(const std::string& directory_path){
    std::vector<std::string> files = {
            "px.dds",
            "nx.dds",
            "py.dds",
            "ny.dds",
            "pz.dds",
            "nz.dds"
    };

    GLuint cube_textureID;
    glGenTextures(1, &cube_textureID);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cube_textureID);

    for (int i = 0; i < files.size(); i++){
        std::ifstream ddsFile (directory_path + files[i], std::ios::in | std::ios::binary);

//        dprint("File : " + directory_path + files[i], 0);
        if (!ddsFile.is_open()) {
            dprint("Unable to find file : " + directory_path + files[i], 2);
            ddsFile.close();
            return -1;
        }

        char format_code[4];
        ddsFile.read(format_code, 4);
        if (strncmp(format_code, "DDS ", 4) != 0){
            dprint("Loaded file is not a DDS file", 2);
            ddsFile.close();
            return -1;
        }

        char header[124];
        ddsFile.read(header, 124);

        unsigned int height = *(unsigned int*)&(header[8]);
        unsigned int width = *(unsigned int*)&(header[12]);
        unsigned int linearSize = *(unsigned int*)&(header[16]);
        unsigned int mipMapCount = *(unsigned int*)&(header[24]);
        unsigned int fourCC = *(unsigned int*)&(header[80]);

        unsigned int buffsize = mipMapCount > 1 ? linearSize*2: linearSize;

        auto buffer = (char *)malloc(buffsize * sizeof(char));
        ddsFile.read(buffer, buffsize);
        ddsFile.close();

        unsigned int format;
        switch(fourCC)
        {
            case FOURCC_DXT1:
                format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
                break;
            case FOURCC_DXT3:
                format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
                break;
            case FOURCC_DXT5:
                format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
                break;
            default:
                free(buffer);
                return 0;
        }

        unsigned int blockSize = (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16;
        unsigned int offset = 0;

        for (unsigned int level = 0; level < mipMapCount; ++level) {
            if(width == 0 || height == 0) { // discard any odd mipmaps 0x1 0x2 resolutions
                mipMapCount--;
                continue;
            }
            unsigned int size = ((width+3)/4)*((height+3)/4)*blockSize;
            glCompressedTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, level, format, width, height,
                                   0, size, buffer + offset);

            offset += size;
            width  /= 2;
            height /= 2;
        }

        free(buffer);
        glTexParameteri(GL_TEXTURE_CUBE_MAP_ARRAY, GL_TEXTURE_MAX_LEVEL, mipMapCount-1);
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_LEVEL, 0);

    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    return cube_textureID;
}