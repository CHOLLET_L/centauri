#ifndef CENTAURI_APP_H
#define CENTAURI_APP_H

#include <SDL.h>
#include <vector>

#include "Data_Structure.h"
#include "GUI.h"
#include "../Scene/Scene.h"

class App {
public:
    App();
    void Update(double delta, bool tick);
    void Render(double delta);

    ~App();

    bool isQuit() const;
    void setQuit(bool quit);

    void menu_behavior();
    static void reset_mouse_pos();

    Scene* scene;
    Data_Structure config;

private:
    void update_event();

    SDL_Window *window;
    SDL_GLContext context;

    SDL_Event e;

    GUI* gui;

    std::vector<double> FPS_tab;

    int in_menu = 0;

    bool in_scene = true;
    bool quit = false;
};


#endif //CENTAURI_APP_H
