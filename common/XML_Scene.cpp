#include <libxml++/libxml++.h>

#include "../common/Data_Structure.h"

#include "../Scene/Scene.h"
#include "../Scene/Sphere.h"

#include "../Scene/Properties_physics.h"
#include "../Scene/SkySphere.h"


object_info obj_from_node(xmlpp::Element* elem){
    object_info param;
    xmlpp::Node* current_obj_attr = elem->get_first_child();
    for (; current_obj_attr; current_obj_attr = current_obj_attr->get_next_sibling()) {
        auto* set = dynamic_cast<xmlpp::Element*>(current_obj_attr);
        if (!set) continue;

        std::string name = set->get_name();
        float val = std::stof(set->get_attribute("value")->get_value());
        if (name == "Size") {
            param.scale = val;
        } else if (name == "Pos_x") {
            param.position[0] = val;
        } else if (name == "Pos_y") {
            param.position[1] = val;
        } else if (name == "Pos_z") {
            param.position[2] = val;
        } else if (name == "Colatitude") {
            param.colatitude = (float) (val * (M_PI / 180));
        } else if (name == "Longitude") {
            param.longitude = (float) (val * (M_PI / 180));
        } else if (name == "RotSpeed") {
            param.rot_speed_lg = val;
        }
    }
    return param;
}

material_info mat_from_node(xmlpp::Element* elem){
    material_info param;
    xmlpp::Node* current_obj_attr = elem->get_first_child();
    for (; current_obj_attr; current_obj_attr = current_obj_attr->get_next_sibling()) {
        auto* set = dynamic_cast<xmlpp::Element*>(current_obj_attr);
        if (!set) continue;

        std::string name = set->get_name();
        float val = std::stof(set->get_attribute("value")->get_value());
        if (name == "Diffuse") {
            param.diffuse = val;
        } else if (name == "Ambient") {
            param.ambient = val;
        }
    }
    return param;
}

physical_info phy_from_node(xmlpp::Element* elem){
    physical_info phy;
    xmlpp::Node* current_obj_attr = elem->get_first_child();
    for (; current_obj_attr; current_obj_attr = current_obj_attr->get_next_sibling()) {
        auto* set = dynamic_cast<xmlpp::Element*>(current_obj_attr);
        if (!set) continue;

        std::string name = set->get_name();
        double val = std::stod(set->get_attribute("value")->get_value());
        if (name == "Mass") {
            phy.mass = val;
        } else if (name == "Speed_x") {
            phy.speed[0] = val;
        } else if (name == "Speed_y") {
            phy.speed[1] = val;
        } else if (name == "Speed_z") {
            phy.speed[2] = val;
        }
    }
    return phy;
}

light_info light_from_node(xmlpp::Element* elem, std::string &link){
    light_info li;
    xmlpp::Node* current_obj_attr = elem->get_first_child();
    for (; current_obj_attr; current_obj_attr = current_obj_attr->get_next_sibling()) {
        auto* set = dynamic_cast<xmlpp::Element*>(current_obj_attr);
        if (!set) continue;

        std::string name = set->get_name();
        std::string str_val = set->get_attribute("value")->get_value();

        if (name == "link") {
            link = str_val;
        }else {
            double val = std::stod(str_val);
            if (name == "Light_r") {
                li.light_color[0] = val;
            } else if (name == "Light_g") {
                li.light_color[1] = val;
            } else if (name == "Light_b") {
                li.light_color[2] = val;
            } else if (name == "Ambient_r") {
                li.ambient_color[0] = val;
            } else if (name == "Ambient_g") {
                li.ambient_color[1] = val;
            } else if (name == "Ambient_b") {
                li.ambient_color[2] = val;
            }
        }
    }
    return li;
}

void Scene::load_from_data() {
    int s = objects.size();
    for (int i = 0; i<s ; ++i){
        delete objects[i];
    }
    objects.clear();

    s = scene_object::lights.size();
    for (int i = 0; i<s ; ++i){
        delete scene_object::lights[i];
    }
    scene_object::lights.clear();
    
    xmlpp::NodeSet nodes = config.get_type_xpath("SceneObject");

    xmlpp::Element* current_object = nullptr;
    int size = nodes.size();
    for (int i = 0; i < size; ++i){
        current_object = dynamic_cast<xmlpp::Element*>(nodes[i]);
//        print_node(nodes[0]);
//        exit(0);
        if (current_object){
            std::string type = current_object->get_attribute("type")->get_value();
            xmlpp::Node* current_obj_attr = current_object->get_first_child();
//            if (dynamic_cast<xmlpp::Element*>(current_obj_attr)) continue;
            if (type == "Sphere") {
                std::string name;
                std::string texture_path;
                object_info ob_in = object_info();
                material_info mat_in = material_info();
                physical_info ph_in = physical_info();
                int detail = 0;
                for (; current_obj_attr; current_obj_attr = current_obj_attr->get_next_sibling()) {
                    auto* set = dynamic_cast<xmlpp::Element*>(current_obj_attr);
                    if (!set) continue;

                    std::string elem_name = set->get_name();
//                    dprint(elem_name, 0);
                    if (elem_name == "Name") {
                        name = set->get_attribute("value")->get_value();
//                    dprint(name, 0);
                    } else if (elem_name == "ObjInfo") {
                        ob_in = obj_from_node(set);
                    } else if (elem_name == "Texture") {
                        texture_path = set->get_attribute("value")->get_value();
                    } else if (elem_name == "Material") {
                        mat_in = mat_from_node(set);
                    } else if (elem_name == "Phys") {
                        ph_in = phy_from_node(set);
                    } else if (elem_name == "Detail") {
                        detail = std::stoi(set->get_attribute("value")->get_value());
                    }
                }
                int a = objects.size();

                objects.push_back(new Sphere(name, ob_in, texture_path, mat_in, ph_in));
                objects[a]->change_lod(detail);
            }
            if (type == "SkySphere") {
                std::string texture_path;
                int detail = 0;
                for (; current_obj_attr; current_obj_attr = current_obj_attr->get_next_sibling()) {
                    auto* set = dynamic_cast<xmlpp::Element*>(current_obj_attr);
                    if (!set) continue;

                    std::string elem_name = set->get_name();
                    if (elem_name == "Texture") {
                        texture_path = set->get_attribute("value")->get_value();
                    } else if (elem_name == "Detail") {
                        detail = std::stoi(set->get_attribute("value")->get_value());
                    }
                }
                int a = objects.size();
                objects.push_back(new SkySphere(texture_path));
                objects[a]->change_lod(detail);
            }
        }
    }

    nodes = config.get_type_xpath("Light");
    current_object = nullptr;
    size = nodes.size();
    for (int i = 0; i < size; ++i){
        current_object = dynamic_cast<xmlpp::Element*>(nodes[i]);
        if (current_object){
            std::string link;
            light_info li = light_from_node(current_object, link);
            if (!link.empty()){
                delete li.light_pos;
                li.light_pos = &objects[get_id_by_name(link)]->getParams()->position;
            }
            scene_object::lights.push_back(new light_info{li});
        }
    }
//    exit(0);

}

void Scene::set_to_data() {

}
