#ifndef CENTAURI_LOG_H
#define CENTAURI_LOG_H

#include <iostream>
#include <ctime>

#include <glm/glm.hpp>

class Log {
public:
    Log(int log_level);
    Log(int log_level, const std::string& output_stream);
    void write(const std::string& file, const std::string& function, int line, int level, std::string msg, std::time_t time);
    void write(const std::string& file, const std::string& function, int line, int level, double msg, std::time_t time);
    void write(const std::string& file, const std::string& function, int line, int level, int msg, std::time_t time);
    void write(const std::string& file, const std::string& function, int line, int level, glm::mat4 msg, std::time_t time);
//    void write(const std::string& file, const std::string& function, int line, int level, void msg, std::time_t time);
//    void write(const std::string& file, const std::string& function, int line, int level, const void* ref, std::time_t time);

private:
    int desired_level;
    std::string output_file;
    bool to_cout;
};

extern Log logger;

std::string ptr_to_str(const void* ptr);

#define CENTAURI_LOG

#ifdef CENTAURI_LOG
#define dprint(msg, level); logger.write(__FILE__, __func__, __LINE__, level, msg, std::time(0))
//#define dprint(msg, level) logger.write(__FILE__, __PRETTY_FUNCTION__, __LINE__, level, msg, std::time(0))
#else
#define dprint(msg, level);
#endif //CENTAURI_LOG

#endif //CENTAURI_LOG_H
