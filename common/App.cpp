#include <GL/glew.h>
#include <string>
#include <SDL.h>

#include <CEGUI/CEGUI.h>

#include "App.h"
#include "log.h"
#include "Properties_app.h"


App::App() {
//// Initialize Config
    config.load_from_file("../common/config.xml");

//// Initialize SDL2
    if (SDL_Init(SDL_INIT_VIDEO) != 0){
        dprint(std::string("SDL_Init Error: ") + SDL_GetError(), 2);
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 2);

//    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
//    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

    window = SDL_CreateWindow("Centauri", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
//    window = SDL_CreateWindow("Centauri", 0, 0,
                                  std::stoi(config.get_val_xpath("WINDOW_WIDTH"), nullptr), std::stoi(config.get_val_xpath("WINDOW_HEIGHT"), nullptr), SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);

    if (window == nullptr){
        dprint(std::string("SDL_CreateWindow Error: ") + SDL_GetError(), 2);
        SDL_Quit();
    }

    context = SDL_GL_CreateContext(window);
    if (context == nullptr){
        dprint(std::string("SDL_CreateContext Error: ") + SDL_GetError(), 2);
    }

    SDL_SetRelativeMouseMode((SDL_bool)true);

//    SDL_EnableUNICODE(1); //

    SDL_GL_SetSwapInterval(0); // Disabling V-sync

///// Initialize OpenGL with GLEW
    if (glewInit() != GLEW_OK) {
        dprint("Failed to initialize GLEW", 2);
        exit(-1);
    }

    glClearColor(1.0, 1.0, 0.80, 1.0);

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

//    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // Draws Triangle

//// Initialize CEGUI
    gui = new GUI;
    gui->initialize();
    gui->set_parent_pointer(this);
    gui->resizeGUI(std::stoi(config.get_val_xpath("WINDOW_WIDTH"), nullptr), std::stoi(config.get_val_xpath("WINDOW_HEIGHT"), nullptr));

//// Initialize Scene
    scene = new Scene();
    scene->camera->set_window_size(
            std::stoi(config.get_val_xpath("WINDOW_WIDTH"), nullptr),
            std::stoi(config.get_val_xpath("WINDOW_HEIGHT"), nullptr));
}

void App::Update(double delta, bool tick) {
    GUI::inject_CEGUI_time((float) delta);
    update_event();

    scene->Update_scene(delta);

    FPS_tab.push_back(delta);
    if (tick){
        double moy = 0;
        for (double i : FPS_tab)    moy += i;
        moy /= FPS_tab.size();
        FPS_tab.clear();
        gui->FPS_counter->setText(std::to_string(int(1 / moy)) + " FPS");
    }
}

void App::menu_behavior(){
    in_menu = gui->opened_menu();
    if (in_menu == 0) {
        in_scene = true;
        SDL_SetRelativeMouseMode((SDL_bool) true);
        reset_mouse_pos();
    } else {
        in_scene = false;
        SDL_SetRelativeMouseMode((SDL_bool) false);
    }
}

void App::update_event() {
    const unsigned char *key = SDL_GetKeyboardState(nullptr);

    while(SDL_PollEvent(&e)) {
        GUI::inject_CEGUI_event(&e);
        switch (e.type) {
            case SDL_QUIT:
                quit = true;
                break;
            case SDL_KEYDOWN:
                if (e.key.keysym.sym == SDLK_F1){
                    gui->game_menu->setVisible(!gui->game_menu->isVisible());
                    gui->Scene_Tab->setDisabled(!gui->Scene_Tab->isDisabled());
                    menu_behavior();
                }
                if (e.key.keysym.sym == SDLK_F5){
                    gui->FPS_counter->setVisible(!gui->FPS_counter->isVisible());
                }
                if (e.key.keysym.sym == SDLK_F10){
                    quit = true;
                }
                if (e.key.keysym.sym == SDLK_m){
                    if (!gui->game_menu->isVisible()) {
                        gui->Scene_Tab->setVisible(!gui->Scene_Tab->isVisible());
                        menu_behavior();
                    }
                }
                if (e.key.keysym.sym == SDLK_ESCAPE){
                    if (gui->Scene_Tab->isVisible() and !gui->game_menu->isVisible()){
                        gui->Scene_Tab->setVisible(false);
                    } else {
                        if (gui->game_menu->actual_menu == Main){
                            gui->game_menu->setVisible(!gui->game_menu->isVisible());
                            gui->Scene_Tab->setDisabled(!gui->Scene_Tab->isDisabled());
                        } else {
                            gui->game_menu->load_menu(Main);
                        }
                    }
                    menu_behavior();
                }
				if (e.key.keysym.sym == SDLK_q) {
                    quit = true;
                }
                if (e.key.keysym.sym == SDLK_TAB) {
                    int offset = 1;
                    if (key[SDL_SCANCODE_LSHIFT] or key[SDL_SCANCODE_RSHIFT]) offset = -1;
                    int id = 0;
                    if (scene->camera->is_bind()) {
                        id = scene->get_closest_obj(*scene->camera->getRelOrigin()) + offset;
                        if (id > int(scene->getObjects().size() - 2) ) id = 0; // Doesn't count SkyBox
                        if (id < 0) id = scene->getObjects().size() - 2;
                    }
//                    dprint(scene->getObjects()[id]->getName() + ": " + std::to_string(id), 0);
                    scene->camera->set_animation(
                            &(scene->getObjects()[id]->getParams()->position),
                            scene->getObjects()[id]->getParams()->scale
                    );
                }
                if (e.key.keysym.sym == SDLK_0 || e.key.keysym.sym == SDLK_KP_0) {
                    scene->camera->setRelOrigin(scene->origin);
                }

                break;
            case SDL_MOUSEBUTTONDOWN:
                if (e.button.button == SDL_BUTTON_RIGHT){
                    if (! gui->game_menu->isVisible()) {
                        gui->Scene_Tab->setVisible(!gui->Scene_Tab->isVisible());
                        menu_behavior();
                    }
                }

        }
    }
    int mouse_x = 0, mouse_y = 0, win_x = 0, win_y = 0;
    SDL_GetGlobalMouseState(&mouse_x, &mouse_y);
    SDL_GetWindowPosition(window, &win_x, &win_y);
    GUI::inject_CEGUI_Mouse((float) (mouse_x - win_x), (float) (mouse_y - win_y));

    if (in_scene) {
        int mouseX = 0;
        int mouseY = 0;
        int mvt[] = {0, 0, 0};

        if (key[SDL_SCANCODE_LSHIFT] or key[SDL_SCANCODE_RSHIFT]) {
            scene->set_camera_speed_coef(0.1);
        } else if (key[SDL_SCANCODE_LCTRL] or key[SDL_SCANCODE_RCTRL]) {
            scene->set_camera_speed_coef(1.5);
        } else {
            scene->set_camera_speed_coef(1);
        }
        if (key[SDL_SCANCODE_UP]) {
            mvt[0] = 1;
        }
        if (key[SDL_SCANCODE_DOWN]) {
            mvt[0] = -1;
        }
        if (key[SDL_SCANCODE_RIGHT]) {
            mvt[1] = 1;
        }
        if (key[SDL_SCANCODE_LEFT]) {
            mvt[1] = -1;
        }
        if (key[SDL_SCANCODE_PAGEUP]) {
            mvt[2] = 1;
        }
        if (key[SDL_SCANCODE_PAGEDOWN]) {
            mvt[2] = -1;
        }

        SDL_GetRelativeMouseState(&mouseX, &mouseY);
        scene->camera->set_mouse_mvt(-mouseX, -mouseY);

        scene->camera->set_mvt(mvt[0], mvt[1], mvt[2]);
    }
//    std::stringstream ss;
//    ss << "X: " << mouseX << "  Y: " << mouseY;
//    SDL_SetWindowTitle(window, ss.str().c_str());
}

void App::Render(double delta) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // NOLINT(hicpp-signed-bitwise)

    scene->Render(delta);

    CEGUI::System::getSingleton().renderAllGUIContexts();

    SDL_GL_SwapWindow(window);

}

bool App::isQuit() const {
    return quit;
}

void App::setQuit(bool qu) {
        quit = qu;
}

void App::reset_mouse_pos() {
    SDL_GetRelativeMouseState(nullptr, nullptr);
}

App::~App() {
    delete gui;
    delete scene;

    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(window);
    SDL_Quit();
}