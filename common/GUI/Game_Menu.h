#ifndef CENTAURI_GAME_MENU_H
#define CENTAURI_GAME_MENU_H

#include <CEGUI/CEGUI.h>

enum Menus {Main, Options};

class Game_Menu {
public:
    Game_Menu(CEGUI::Window* root);
    ~Game_Menu();

    CEGUI::DefaultWindow* getMainWindow();

    bool isVisible() const;
    void setVisible(bool m_visible);

    void load_menu(Menus menu);


    Menus actual_menu = Main;

private:
    CEGUI::Window* root;
    CEGUI::DefaultWindow* Escape_Menu;

    CEGUI::DefaultWindow* Container;

    CEGUI::VerticalLayoutContainer* Escape_Menu_main_VLC;
    CEGUI::VerticalLayoutContainer* Escape_Menu_opt_VLC;

};


#endif //CENTAURI_GAME_MENU_H
