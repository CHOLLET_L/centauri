#include "Game_Menu.h"


Game_Menu::Game_Menu(CEGUI::Window* m_root) : root(m_root) {
    Escape_Menu = static_cast<CEGUI::DefaultWindow*>(
            CEGUI::WindowManager::getSingleton().loadLayoutFromFile("Escape_Menu.xml"));
    root->addChild(Escape_Menu);

    Container = static_cast<CEGUI::DefaultWindow*>(Escape_Menu->getChild("Container"));
    Escape_Menu_main_VLC = static_cast<CEGUI::VerticalLayoutContainer*>(Escape_Menu->getChildRecursive("main_VLC"));
    Escape_Menu_opt_VLC = static_cast<CEGUI::VerticalLayoutContainer*>(Escape_Menu->getChildRecursive("opt_VLC"));

    Escape_Menu_main_VLC->layout();
    Escape_Menu_opt_VLC->layout();

    Container->addChild(Escape_Menu_main_VLC);
    Container->getChild(0)->setVisible(true);
}

CEGUI::DefaultWindow *Game_Menu::getMainWindow() {
    return Escape_Menu;
}

bool Game_Menu::isVisible() const {
    return Escape_Menu->isVisible();
}

void Game_Menu::setVisible(bool m_visible) {
    Escape_Menu->setVisible(m_visible);
    Container->setVisible(m_visible);
}

void Game_Menu::load_menu(Menus menu) {
    actual_menu = menu;
    Container->removeChild(Container->getChild(0));
    if (menu == Main) {
        Container->addChild(Escape_Menu_main_VLC);
    } else if (menu == Options){
        Container->addChild(Escape_Menu_opt_VLC);
    }
    Container->getChild(0)->setVisible(true);
}

Game_Menu::~Game_Menu() {
    // root destructor called by parent GUI destructor
    delete Escape_Menu;
    delete Container;
    delete Escape_Menu_main_VLC;
    delete Escape_Menu_opt_VLC;
}
