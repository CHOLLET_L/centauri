#ifndef CENTAURI_GUI_H
#define CENTAURI_GUI_H

#include <SDL.h>

#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/OpenGL/GL3Renderer.h>

#include "GUI/Game_Menu.h"
#include "Properties_app.h"

class App;
class GUI;
#include "App.h"


class GUI {
public:
    GUI() = default;
    ~GUI();
    void initialize();
    void set_parent_pointer(App*);

    void resizeGUI(int x, int y);

    static void inject_CEGUI_event(SDL_Event* e);
    static void inject_CEGUI_Mouse(float x, float y);
    static void inject_CEGUI_time(float delta);

    int opened_menu();

    bool onBt_terre(const CEGUI::EventArgs& e);

    bool onBt_resume(const CEGUI::EventArgs& e);
    bool onBt_opt(const CEGUI::EventArgs& e);
    bool onBt_quit(const CEGUI::EventArgs& e);

    CEGUI::DefaultWindow* FPS_counter;
    CEGUI::DefaultWindow* Scene_Tab;

    Game_Menu* game_menu;

private:
    CEGUI::OpenGL3Renderer* Renderer;
    CEGUI::Window* RootWindow;

    App* pointer_app;
};

#endif //CENTAURI_GUI_H
