#include "../common/log.h"
#include "Data_Structure.h"


// From libxml++ examples
//#include <glibmm/convert.h>
//
//std::ostream& operator<<(std::ostream& os, const CatchConvertError& utf8_string)
//{
//    try
//    {
//        os << static_cast<const Glib::ustring&>(utf8_string);
//    }
//    catch (const Glib::ConvertError& ex)
//    {
//        os << "[Glib::ConvertError: " << ex.what() << "]";
//    }
//    return os;
//}

//void print_node(const xmlpp::Node* node, unsigned int indentation)
//{
//    const Glib::ustring indent(indentation, ' ');
//    std::cout << std::endl; //Separate nodes by an empty line.
//
//    const auto nodeContent = dynamic_cast<const xmlpp::ContentNode*>(node);
//    const auto nodeText = dynamic_cast<const xmlpp::TextNode*>(node);
//    const auto nodeComment = dynamic_cast<const xmlpp::CommentNode*>(node);
//
//    if(nodeText && nodeText->is_white_space()) //Let's ignore the indenting - you don't always want to do this.
//        return;
//
//    const auto nodename = node->get_name();
//
//    if(!nodeText && !nodeComment && !nodename.empty()) //Let's not say "name: text".
//    {
//        const auto namespace_prefix = node->get_namespace_prefix();
//
//        std::cout << indent << "Node name = ";
//        if(!namespace_prefix.empty())
//            std::cout << CatchConvertError(namespace_prefix) << ":";
//        std::cout << CatchConvertError(nodename) << std::endl;
//    }
//    else if(nodeText) //Let's say when it's text. - e.g. let's say what that white space is.
//    {
//        std::cout << indent << "Text Node" << std::endl;
//    }
//
//    //Treat the various node types differently:
//    if(nodeText)
//    {
//        std::cout << indent << "text = \"" << CatchConvertError(nodeText->get_content()) << "\"" << std::endl;
//    }
//    else if(nodeComment)
//    {
//        std::cout << indent << "comment = " << CatchConvertError(nodeComment->get_content()) << std::endl;
//    }
//    else if(nodeContent)
//    {
//        std::cout << indent << "content = " << CatchConvertError(nodeContent->get_content()) << std::endl;
//    }
//    else if(const xmlpp::Element* nodeElement = dynamic_cast<const xmlpp::Element*>(node))
//    {
//        //A normal Element node:
//
//        //line() works only for ElementNodes.
//        std::cout << indent << "     line = " << node->get_line() << std::endl;
//
//        //Print attributes:
//        const auto attributes = nodeElement->get_attributes();
//        for(xmlpp::Element::AttributeList::const_iterator iter = attributes.begin(); iter != attributes.end(); ++iter)
//        {
//            const auto attribute = *iter;
//            const auto namespace_prefix = attribute->get_namespace_prefix();
//
//            std::cout << indent << "  Attribute ";
//            if(!namespace_prefix.empty())
//                std::cout << CatchConvertError(namespace_prefix) << ":";
//            std::cout << CatchConvertError(attribute->get_name()) << " = "
//                      << CatchConvertError(attribute->get_value()) << std::endl;
//        }
//
//        const auto attribute = nodeElement->get_attribute("title");
//        if(attribute)
//        {
//            std::cout << indent;
//            if (dynamic_cast<const xmlpp::AttributeNode*>(attribute))
//                std::cout << "AttributeNode ";
//            else if (dynamic_cast<const xmlpp::AttributeDeclaration*>(attribute))
//                std::cout << "AttributeDeclaration ";
//            std::cout << "title = " << CatchConvertError(attribute->get_value()) << std::endl;
//        }
//    }
//
//    if(!nodeContent)
//    {
//        //Recurse through child nodes:
//        auto list = node->get_children();
//        for(const auto& child : node->get_children())
//        {
//            print_node(child, indentation + 2); //recursive
//        }
//    }
//}

Data_Structure::Data_Structure(const std::string& m_filename) {
    parser.parse_file(m_filename);
    root = parser.get_document()->get_root_node();
}

Data_Structure::Data_Structure(const Data_Structure &) {
    dprint("Unexpected comportment: " + ptr_to_str(this), 2);
}

Data_Structure &Data_Structure::operator=(const Data_Structure &) {
    dprint("Unexpected comportment: " + ptr_to_str(this), 2);
    return *this;
}

void Data_Structure::load_from_file(const std::string& m_filename) {
    parser.parse_file(m_filename);
    root = parser.get_document()->get_root_node();
}

void Data_Structure::save_to_file(const std::string& m_filename) {
    auto *document = parser.get_document();
    if(document)
        document->write_to_file(m_filename);
}

void Data_Structure::set_val_xpath(const std::string& xpath, const std::string& val) {
    auto set = root->find("//" + xpath);

    for(const auto& child : set)
    {
        auto* element = dynamic_cast<xmlpp::Element*>(child);
        if (element) element->set_attribute("value", val);
    }
}

std::string Data_Structure::get_val_xpath(const std::string& xpath) {
    auto set = root->find("//" + xpath);
    std::string val;
    for(const auto& child : set)
    {
        auto* element = dynamic_cast<xmlpp::Element*>(child);
        if (element) {
            val = element->get_attribute("value")->get_value();
        }
    }
    return val;
}

xmlpp::NodeSet Data_Structure::get_type_xpath(const std::string& xpath) {
    return root->find(xpath);
}
