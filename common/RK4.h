#ifndef CENTAURI_RK4_H
#define CENTAURI_RK4_H

#include <vector>
#include "../Scene/scene_object.h"

void update_position(std::vector<scene_object *> &m_scene, double delta_t);

#endif //CENTAURI_RK4_H
