#ifndef CENTAURI_DATA_STRUCTURE_H
#define CENTAURI_DATA_STRUCTURE_H

#include <string>

#include <libxml++/libxml++.h>

class Data_Structure {
public:
    Data_Structure() = default;
    explicit Data_Structure(const std::string&);
    Data_Structure(const Data_Structure &);
    ~Data_Structure() = default;
    Data_Structure &operator=(const Data_Structure &);

    void load_from_file(const std::string&);
    void save_to_file(const std::string&);

    void set_val_xpath(const std::string&, const std::string&);
    std::string get_val_xpath(const std::string&);

    xmlpp::NodeSet get_type_xpath(const std::string&);

private:
    xmlpp::DomParser parser;
    xmlpp::Node* root{};
};


class CatchConvertError : public Glib::ustring
{
public:
    explicit CatchConvertError(const Glib::ustring& str)
            : Glib::ustring(str)
    { }
};

// From libxml++ examples
//std::ostream& operator<<(std::ostream& os, const CatchConvertError& utf8_string);
//void print_node(const xmlpp::Node* node, unsigned int = 0);

#endif //CENTAURI_DATA_STRUCTURE_H
