#include <vector>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/norm.hpp>

#include "../Scene/scene_object.h"
#include "../Scene/Properties_physics.h"
#include "RK4.h"

double dt = 1; // Day by second


double force_g(double mb, glm::dvec3 dist){
    return (G * mb) / glm::length2(dist);
}

std::vector<glm::dvec3> acc(std::vector<scene_object *> &m_scene){
    int lenght = m_scene.size();
    std::vector<glm::dvec3> force_sum(lenght, {0, 0, 0});

    for (int i = 0; i < lenght; i++){
        if (m_scene[i]->getPhys()->mass==0) continue;
        for (int j = 0; j < lenght; j++){
            if ((i==j) or m_scene[j]->getPhys()->mass==0) continue;
//            std::cout << j << " " << m_scene[j]->getPhys()->mass << std::endl;
            glm::dvec3 dist = m_scene[j]->getParams()->position - m_scene[i]->getParams()->position;
            glm::dvec3 unit = glm::normalize(dist);
            force_sum[i] += unit * force_g(m_scene[j]->getPhys()->mass, dist);
        }
    }

    return force_sum;
}

void update_position(std::vector<scene_object *> &m_scene, double delta_t){
    std::vector<glm::dvec3> force = acc(m_scene);
// Euler
    for (int i = 0; i < m_scene.size(); i++){
        if (m_scene[i]->getPhys()->mass==0) continue;
        m_scene[i]->getParams()->position += delta_t * m_scene[i]->getPhys()->speed;
        m_scene[i]->getPhys()->speed += delta_t * force[i];
    }
}