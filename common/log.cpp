#include <fstream>
#include <string>
#include <ctime>
#include <iomanip>
#include <iostream>
//#include <ios>

#include "log.h"

Log logger(0);

Log::Log(int log_level) : to_cout(true), desired_level(log_level) {}

Log::Log(int log_level, const std::string& o_stream) : desired_level(log_level){
    to_cout = o_stream.empty();
    if (!to_cout){
        output_file = o_stream;

        std::ifstream fichier_i(o_stream);
        if (!fichier_i.fail()){
            fichier_i.close();
            std::ofstream fichier_o(o_stream);
        }
    }
}

void Log::write(const std::string& file, const std::string& function, const int line, const int level, const std::string msg, const std::time_t time) {
//void write(const std::string& file, const std::string& function, int line, int level, const std::string& msg, std::time_t time) {
    if (level < desired_level) return;

    std::string print_level;
    switch(level){
        case 0:
            print_level = "INFO";
            break;
        case 1:
            print_level = "WARN";
            break;
        case 2:
            print_level = "ERROR";
            break;
    }

    std::tm tm = *std::localtime(&time);

    if (!output_file.empty()) {
        std::ofstream of_stream (output_file, std::ios::app);
        if (of_stream.is_open()){
            of_stream <<  "[" << std::put_time(&tm, "%H:%M:%S") << "]" <<
                         "[" << print_level << "]\t"
                         << file << "(" << line << ") " << function << " : "
                         << msg << std::endl;
        }
        else{
            std::cerr << "Unable to open log file" << std::endl;
        }
    }
    else{
        std::cout <<  "[" << std::put_time(&tm, "%H:%M:%S") << "]" <<
                  "[" << print_level << "]\t"
                  << file << "(" << line << ") " << function << " : "
                  << msg << std::endl;
    }
}

void Log::write(const std::string& file, const std::string& function, const int line, const int level, const double msg, const std::time_t time) {
    write(file, function, line, level, std::to_string(msg), time);
}

void Log::write(const std::string& file, const std::string& function, const int line, const int level, const int msg, const std::time_t time) {
    write(file, function, line, level, std::to_string(msg), time);
}

void Log::write(const std::string& file, const std::string& function, const int line, const int level, const glm::mat4 msg, const std::time_t time) {
    write(file, function, line, level, 
            "\n" + std::to_string(msg[0][0]) + " " + std::to_string(msg[0][1]) + " " + std::to_string(msg[0][2]) + " " + std::to_string(msg[0][3]) + "\n"
                 + std::to_string(msg[1][0]) + " " + std::to_string(msg[1][1]) + " " + std::to_string(msg[1][2]) + " " + std::to_string(msg[1][3]) + "\n"
                 + std::to_string(msg[2][0]) + " " + std::to_string(msg[2][1]) + " " + std::to_string(msg[2][2]) + " " + std::to_string(msg[2][3]) + "\n"
                 + std::to_string(msg[3][0]) + " " + std::to_string(msg[3][1]) + " " + std::to_string(msg[3][2]) + " " + std::to_string(msg[3][3]) + "\n", time);
}

//void Log::write(const std::string& file, const std::string& function, int line, int level, const void* ref, std::time_t time) {
//
//    write(file, function, line, level, ptr_to_str(ref), time);
//}


std::string ptr_to_str(const void* ptr){
    std::stringstream ss;
    ss << ptr;
    std::string out;
    ss >> out;
    return out;
}
//Log logger(0, "logtest.txt");