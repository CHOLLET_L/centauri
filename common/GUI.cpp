#include <SDL_events.h>

#include "GUI.h"
#include "log.h"

void GUI::initialize() {
//// Initialize CEGUI

    Renderer = &CEGUI::OpenGL3Renderer::bootstrapSystem();

    auto* rp = dynamic_cast<CEGUI::DefaultResourceProvider *>(CEGUI::System::getSingleton().getResourceProvider());
    rp->setResourceGroupDirectory("schemes", "../common/GUI/schemes");
    rp->setResourceGroupDirectory("imagesets", "../common/GUI/imagesets/");
    rp->setResourceGroupDirectory("fonts", "../common/GUI/fonts/");
    rp->setResourceGroupDirectory("layouts", "../common/GUI/layouts/");
    rp->setResourceGroupDirectory("looknfeels", "../common/GUI/looknfeel/");

    CEGUI::ImageManager::setImagesetDefaultResourceGroup("imagesets");
    CEGUI::Font::setDefaultResourceGroup("fonts");
    CEGUI::Scheme::setDefaultResourceGroup("schemes");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("looknfeels");
    CEGUI::WindowManager::setDefaultResourceGroup("layouts");
//    CEGUI::ScriptModule::setDefaultResourceGroup("lua_scripts");

    CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.xml");
    CEGUI::ImageManager::getSingleton().loadImageset("TaharezLook.xml");
    CEGUI::ImageManager::getSingleton().loadImageset("Centauri.xml");
    RootWindow = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "root");
    CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(RootWindow);
    CEGUI::FontManager::getSingleton().get("DejaVuSans-12").setAutoScaled(CEGUI::ASM_Vertical);

    
    game_menu = new Game_Menu(RootWindow);
    
    
    FPS_counter = static_cast<CEGUI::DefaultWindow*>(
            CEGUI::WindowManager::getSingleton().loadLayoutFromFile("FPS.xml"));
    FPS_counter->setVisible(true);
    RootWindow->addChild(FPS_counter);

    Scene_Tab = static_cast<CEGUI::DefaultWindow*>(
            CEGUI::WindowManager::getSingleton().loadLayoutFromFile("Scene_Tab.xml"));
    RootWindow->addChild(Scene_Tab);

//    VLC = static_cast<CEGUI::VerticalLayoutContainer*>(Escape_Menu->getChildRecursive("VLC"));
//    VLC->layout();
    

    Scene_Tab->getChild("bt_terre")->subscribeEvent(CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber(&GUI::onBt_terre, this));
    game_menu->getMainWindow()->getChildRecursive("Item_rsm")->subscribeEvent(CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber(&GUI::onBt_resume, this));
    game_menu->getMainWindow()->getChildRecursive("Item_opt")->subscribeEvent(CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber(&GUI::onBt_opt, this));
    game_menu->getMainWindow()->getChildRecursive("Item_quit")->subscribeEvent(CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber(&GUI::onBt_quit, this));

}

void GUI::set_parent_pointer(App* app){
    pointer_app = app;
}

void GUI::resizeGUI(int x, int y) {
    CEGUI::Size<float> newSize ((float)x, (float)y);

    Renderer->setDisplaySize(newSize);
    CEGUI::System::getSingleton().notifyDisplaySizeChanged(newSize);

//    pCEGUI_Context->getMouseCursor().notifyDisplaySizeChanged(newSize);

}

bool GUI::onBt_terre(const CEGUI::EventArgs &e) {
//    const auto& we = static_cast<const CEGUI::WindowEventArgs&>(e);
//    std::cout << "Clicked " + we.window->getName() << std::endl;
//    dprint("Zuper bouton !", 0);

    pointer_app->scene->camera->set_animation(
            &(pointer_app->scene->getObjects()[1]->getParams()->position),
            pointer_app->scene->getObjects()[1]->getParams()->scale
            );

    return true;
}

bool GUI::onBt_resume(const CEGUI::EventArgs &e) {
    dprint("Resume", 0);
    game_menu->setVisible(false);
    Scene_Tab->setDisabled(!Scene_Tab->isDisabled());
    pointer_app->menu_behavior();
    return false;
}

bool GUI::onBt_opt(const CEGUI::EventArgs &e) {
    game_menu->load_menu(Options);
    return false;
}

bool GUI::onBt_quit(const CEGUI::EventArgs &e) {
    dprint("Quit", 0);
    pointer_app->setQuit(true);
    return false;
}

void GUI::inject_CEGUI_event(SDL_Event* e) {
    switch (e->type) {
        case SDL_KEYDOWN:
            CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown((CEGUI::Key::Scan)e->key.keysym.sym);
            break;
        case SDL_KEYUP:
            CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp((CEGUI::Key::Scan)e->key.keysym.sym);
            break;
        case SDL_MOUSEBUTTONDOWN:
            switch (e->button.button)
            {
                case SDL_BUTTON_LEFT:
                    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(CEGUI::LeftButton);
                    break;
                case SDL_BUTTON_MIDDLE:
                    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(CEGUI::MiddleButton);
                    break;
                case SDL_BUTTON_RIGHT:
                    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(CEGUI::RightButton);
                    break;
                case SDL_BUTTON_X1:
                    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(CEGUI::X1Button);
                    break;
                case SDL_BUTTON_X2:
                    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(CEGUI::X2Button);
                    break;
            }
            break;
        case SDL_MOUSEBUTTONUP:
            switch (e->button.button)
            {
                case SDL_BUTTON_LEFT:
                    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(CEGUI::LeftButton);
                    break;
                case SDL_BUTTON_MIDDLE:
                    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(CEGUI::MiddleButton);
                    break;
                case SDL_BUTTON_RIGHT:
                    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(CEGUI::RightButton);
                    break;
                case SDL_BUTTON_X1:
                    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(CEGUI::X1Button);
                    break;
                case SDL_BUTTON_X2:
                    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(CEGUI::X2Button);
                    break;
            }
            break;
    }
}

void GUI::inject_CEGUI_Mouse(float x, float y) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(x, y);
}

void GUI::inject_CEGUI_time(float delta) {
    CEGUI::System::getSingleton().injectTimePulse(delta);
}

int GUI::opened_menu() {
    return Scene_Tab->isVisible() + game_menu->isVisible();
}

GUI::~GUI() {
    delete RootWindow;

    delete FPS_counter;
    delete Scene_Tab;
    delete game_menu;
}
