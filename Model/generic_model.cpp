#include "generic_model.h"
#include "../common/log.h"

//generic_model::generic_model() {}

const std::vector<GLfloat> * generic_model::get_vertices() {
    return &vertices_array;
}

const std::vector<GLuint> * generic_model::get_indices() {
    return &indices_array;
}
const std::vector<GLfloat> * generic_model::get_uv() {
    return &uv_array;
}

const std::vector<GLfloat> *generic_model::get_normals() {
    return &normals_array;
}

void generic_model::create_lod(int detail) {
    dprint("Forbidden Virtual method called by " + ptr_to_str(this) ,1);
}

