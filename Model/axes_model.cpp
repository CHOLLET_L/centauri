#include "axes_model.h"

axes_model::axes_model() {
    vertices_array.assign({
                              0.0f, 0.0f, 0.0f,
                              1.0f, 0.0f, 0.0f,
                              0.0f, 0.0f, 0.0f,
                              0.0f, 1.0f, 0.0f,
                              0.0f, 0.0f, 0.0f,
                              0.0f, 0.0f, 1.0f
                          });
    uv_array.assign({
                            255, 0, 0,
                            255, 0, 0,
                            0, 255, 0,
                            0, 255, 0,
                            0, 0, 255,
                            0, 0, 255
                    }); // Colors and not uv in this case

}