#ifndef CENTAURI_AXES_MODEL_H
#define CENTAURI_AXES_MODEL_H

#include "generic_model.h"


class axes_model : public generic_model {
public:
    axes_model();
    ~axes_model() override = default;
};


#endif //CENTAURI_AXES_MODEL_H
