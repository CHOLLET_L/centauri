#ifndef CENTAURI_GENERIC_MODEL_H
#define CENTAURI_GENERIC_MODEL_H


#include <vector>
#include <GL/glew.h>


class generic_model {
public:
    generic_model() = default;

    virtual ~generic_model() = default;

    const std::vector<GLfloat> * get_vertices();
    const std::vector<GLuint> * get_indices();
    const std::vector<GLfloat> * get_uv();
    const std::vector<GLfloat> * get_normals();

    virtual void create_lod(int detail);

protected:
    std::vector<GLuint> indices_array;
    std::vector<GLfloat> vertices_array;
    std::vector<GLfloat> uv_array;
    std::vector<GLfloat> normals_array;
};


#endif //CENTAURI_GENERIC_MODEL_H
