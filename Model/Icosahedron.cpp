#include <vector>
#include <GL/glew.h>
#include <cmath>

#include "Icosahedron.h"
#include "../common/log.h"



// https://github.com/mrdoob/three.js/blob/master/src/geometries/PolyhedronGeometry.js

// Angle around the Y axis, counter-clockwise when looking from above.
float azimuth(const float &x, const float &z){
    return (float)std::atan2(z, -x);
}

// Angle above the XZ plane.
float inclination(const float &x, const float &y, const float &z) {
    return (float)std::atan2( - y, sqrt((double) ((x * x) + (z * z))));
}


Icosahedron::Icosahedron() {
    init_vertices_indices();
    generate_uv();
    generate_normals();
}
Icosahedron::Icosahedron(int detail) {
    init_vertices_indices();
    if (detail > 0) subdivide(detail);
    generate_uv();
    generate_normals();
}

void Icosahedron::create_lod(int detail) {
    if (detail < actual_detail){
        init_vertices_indices();
        subdivide(detail);
    }
    else if (detail > actual_detail)
    {
        subdivide(detail-actual_detail);
    }
}

void Icosahedron::init_vertices_indices() {
    GLfloat t = (1.0+sqrt(5)) / 2.0;
//    vertices_array = {- 1.0, t, 0.0, - t, 0.0, 1.0, 0.0, 1.0, t };
//    indices_array = {0, 1, 2};

    indices_array = {0, 11, 5, 	0, 5, 1, 	0, 1, 7, 	0, 7, 10, 	0, 10, 11,
                     1, 5, 9, 	5, 11, 4,	11, 10, 2,	10, 7, 6,	7, 1, 8,
                     3, 9, 4, 	3, 4, 2,	3, 2, 6,	3, 6, 8,	3, 8, 9,
                     4, 9, 5, 	2, 4, 11,	6, 2, 10,	8, 6, 7,	9, 8, 1};
    vertices_array = {- 1.0, t, 0.0, 	1.0, t, 0.0, 	- 1.0, - t, 0.0, 	1.0, - t, 0.0,
                      0.0, - 1.0, t, 	0.0, 1.0, t,	0.0, - 1.0, - t, 	0.0, 1.0, - t,
                      t, 0.0, - 1.0, 	t, 0.0, 1.0, 	- t, 0.0, - 1.0, 	- t, 0.0, 1.0};

//    indices_array.resize(9);

}

void Icosahedron::add_vector(std::vector<GLfloat> &common_ver, std::vector<GLuint> &common_ind, std::vector<GLfloat> &a) {
    common_ind.push_back((GLuint)(common_ver.size() / 3));

    common_ver.push_back(a[0]);
    common_ver.push_back(a[1]);
    common_ver.push_back(a[2]);

}

void Icosahedron::subdivide(int detail) {

    int end = indices_array.size();

    indices_array.resize(0);
    vertices_array.resize(0);
    temp_indices.reserve(12*3*std::pow(2, detail) + detail);
    temp_vertices.reserve(20*3*std::pow(4, detail));


    for (int i = 0; i < end; i+= 3){
        std::vector<GLfloat> point_a = {vertices_array[3 * indices_array[i+0] + 0],
                                        vertices_array[3 * indices_array[i+0] + 1],
                                        vertices_array[3 * indices_array[i+0] + 2]};
        std::vector<GLfloat> point_b = {vertices_array[3 * indices_array[i+1] + 0],
                                        vertices_array[3 * indices_array[i+1] + 1],
                                        vertices_array[3 * indices_array[i+1] + 2]};
        std::vector<GLfloat> point_c = {vertices_array[3 * indices_array[i+2] + 0],
                                        vertices_array[3 * indices_array[i+2] + 1],
                                        vertices_array[3 * indices_array[i+2] + 2]};
        subdivide_face(point_a, point_b, point_c, detail-actual_detail);
    }



    vertices_array.swap(temp_vertices);
    indices_array.swap(temp_indices);

    temp_vertices.resize(0);
    temp_indices.resize(0);

    normalize_radius();

    generate_uv();
    generate_normals();

//    dprint("Size of vertices_array (Mo): " + std::to_string(vertices_array.size() * sizeof(GLfloat) / 1024 / 1024), 0);
//    dprint("Elements of vertices_array: " + std::to_string(vertices_array.size()), 0)
}

void Icosahedron::subdivide_face(std::vector<GLfloat> &a, std::vector<GLfloat> &b, std::vector<GLfloat> &c, int detail){
    int cols = std::pow(2, detail);

    std::vector<std::vector<std::vector<GLfloat>>> points;
    points.resize(cols+1);

    for (int i = 0; i <= cols; i++){
        int rows = cols - i;

        points[i].resize(rows+1);

        float alpha = ((float)i) / ((float)cols);
        std::vector<GLfloat> aj = {(c[0] - a[0]) * alpha + a[0], (c[1] - a[1]) * alpha + a[1], (c[2] - a[2]) * alpha + a[2]};
        std::vector<GLfloat> bj = {(c[0] - b[0]) * alpha + b[0], (c[1] - b[1]) * alpha + b[1], (c[2] - b[2]) * alpha + b[2]};

        for (int j = 0; j<=rows; j++){
            points[i][j].resize(3);
            float beta = ((float)j) / ((float)rows);
            if (j == 0 && i == cols){
                points[i][j] = aj;
            }
            else {
                points[i][j] = {(bj[0] - aj[0]) * beta + aj[0], (bj[1] - aj[1]) * beta + aj[1], (bj[2] - aj[2]) * beta + aj[2]};
            }
        }
    }

    for (int i = 0; i < cols; ++i ) {

        for (int j = 0; j < 2 * ( cols - i ) - 1; ++j ) {

            int k = j / 2; // implicit convertion / truncate

            if ( j % 2 == 0 ) {
                add_vector(temp_vertices, temp_indices, points[ i ][ k + 1 ]);
                add_vector(temp_vertices, temp_indices, points[ i + 1 ][ k ]);
                add_vector(temp_vertices, temp_indices, points[ i ][ k ]);
            }
            else {
                add_vector(temp_vertices, temp_indices, points[ i ][ k + 1 ]);
                add_vector(temp_vertices, temp_indices, points[ i + 1 ][ k + 1 ]);
                add_vector(temp_vertices, temp_indices, points[ i + 1 ][ k ]);
            }
        }
    }

}


void Icosahedron::generate_uv() {
    uv_array.resize(0);
    uv_array.reserve(vertices_array.size()*2/3);

    for(int i = 0; i < vertices_array.size(); i += 3){
        // OpenGL Convention
//        uv_array.push_back( 0.5 * azimuth(vertices_array[i], vertices_array[i+2]) / M_PI + 0.5);
//        uv_array.push_back(1.0 - (inclination(vertices_array[i], vertices_array[i+1],vertices_array[i+2]) / M_PI + 0.5));

        // DirectX Convention (dds file)
        uv_array.push_back(azimuth(vertices_array[i], vertices_array[i+2]) / 2 / M_PI + 0.5);
        uv_array.push_back(inclination(vertices_array[i], vertices_array[i+1], vertices_array[i+2]) / M_PI + 0.5);
    }

    correct_uv();
}

void Icosahedron::correct_uv() {
    for (int i = 0, j = 0; i < vertices_array.size(); i += 9, j += 6 ) {

        float centroid_x = (vertices_array[i] + vertices_array[i + 3] + vertices_array[i + 6]) / 3;
        float centroid_z = (vertices_array[i+2] + vertices_array[i + 5] + vertices_array[i + 8]) / 3;

        float azi = azimuth(centroid_x, centroid_z);

        for (int k = 0; k < 3; k++) {
            std::vector<GLfloat> point_xyz (vertices_array.begin() + i + 3*k, vertices_array.begin() + i + 3*k + 3);
            std::vector<GLfloat> point_uv (uv_array.begin() + j + 2*k, uv_array.begin() + j + 2*k + 2);

            if ((azi < 0) && ( point_uv[0] == 1 )) {

                uv_array[j+k*2] = point_uv[0] - 1;

            }

            if (( point_xyz[0] == 0 ) && ( point_xyz[2] == 0 )) {

                uv_array[j+k*2] = azi / 2 / M_PI + 0.5;

            }
        }

        // Correct Seam
        float max = std::max(uv_array[j], std::max(uv_array[j+2], uv_array[j+4]));
        float min = std::min(uv_array[j], std::min(uv_array[j+2], uv_array[j+4]));

        if (max > 0.9 && min < 0.1){
            if (uv_array[j] < 0.2) uv_array[j] += 1;
            if (uv_array[j+2] < 0.2) uv_array[j+2] += 1;
            if (uv_array[j+4] < 0.2) uv_array[j+4] += 1;
        }

    }
}


void Icosahedron::normalize_radius() {
    for(int i = 0; i < vertices_array.size(); i+=3){
        float norm = std::sqrt(vertices_array[i]*vertices_array[i]
                + vertices_array[i+1]*vertices_array[i+1]
                + vertices_array[i+2]*vertices_array[i+2]);
        vertices_array[i] /= norm;
        vertices_array[i+1] /= norm;
        vertices_array[i+2] /= norm;
    }
}

void Icosahedron::generate_normals() {
    normals_array.resize(0);
    normals_array.reserve(vertices_array.size());

    for(int i = 0; i < vertices_array.size(); i+=3){
        GLfloat a = vertices_array[i + 0];
        GLfloat b = vertices_array[i + 1];
        GLfloat c = vertices_array[i + 2];

        GLfloat norm = sqrtf(a * a + b * b + c * c);

        normals_array.push_back(a / norm);
        normals_array.push_back(b / norm);
        normals_array.push_back(c / norm);

    }

}
