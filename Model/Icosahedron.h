#ifndef CENTAURI_ICOSAHEDRON_H
#define CENTAURI_ICOSAHEDRON_H

#include <vector>
#include <GL/glew.h>

#include "generic_model.h"

class Icosahedron : public generic_model {
public:
    Icosahedron();
    explicit Icosahedron(int detail);

    void create_lod(int detail) override;

    ~Icosahedron() override = default;

private:

    int actual_detail = 0;

    void subdivide(int detail);


    void init_vertices_indices();

    std::vector<GLfloat> temp_vertices;
    std::vector<GLuint> temp_indices;

    void subdivide_face(std::vector<GLfloat> &a, std::vector<GLfloat> &b, std::vector<GLfloat> &c, int detail);
    void add_vector(std::vector<GLfloat> &common_ver, std::vector<GLuint> &common_ind, std::vector<GLfloat> &a);

    void generate_uv();
    void correct_uv();

    void generate_normals();

    void normalize_radius();
};

#endif //CENTAURI_ICOSAHEDRON_H
