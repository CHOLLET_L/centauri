#version 330 core

in vec2 uv;
in vec3 Normal;
in vec3 FragPos;


out vec4 FragColor;

uniform sampler2D texture_sampler;

struct Light_info {
    vec3 light_pos;

    vec3 light_color;
    vec3 ambient_color;
};

struct Material_info {
    float diffuse;
    float ambient;
};

#define MAX_LIGHT_NUMBER 8
uniform Light_info lights[MAX_LIGHT_NUMBER];

uniform Material_info material;

vec3 Calc_light(Light_info m_light, Material_info m_material,vec3 m_FragPos, vec3 m_norm){
    vec3 lightDir = normalize(m_light.light_pos - m_FragPos);

    float diff = max(dot(m_norm, lightDir), 0.0);
    vec3 diffuse = diff * m_light.light_color;
    return diffuse* material.diffuse + m_light.ambient_color * material.ambient;
}


void main()
{

    vec3 result = vec3(0);
    for (int i = 0; i < MAX_LIGHT_NUMBER; i++){
        result += Calc_light(lights[i], material, FragPos, normalize(Normal)) ;
    }
//    vec3 result = texture(texture_sampler, uv).rgb;
    FragColor = vec4(result* texture(texture_sampler, uv).rgb, 1.0);
}
