#version 330 core

layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 aNormal;

uniform mat4 M;
uniform mat4 VP;
uniform mat4 inv_M;

out vec2 uv;
out vec3 Normal;
out vec3 FragPos;

void main(){
    gl_Position = VP * M * vec4(vertexPosition_modelspace, 1);
    FragPos = vec3(M * vec4(vertexPosition_modelspace, 1.0));
    uv = vertexUV;
    Normal = mat3(inv_M) * aNormal;
}
