#version 330

in vec3 colors;

out vec3 colors_out;

void main() {
//    colors_out = vec3(1, 0, 0);
    colors_out = colors;
}
