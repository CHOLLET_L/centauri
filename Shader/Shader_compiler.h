#ifndef CENTAURI_SHADER_COMPILER_H
#define CENTAURI_SHADER_COMPILER_H

#include <GL/glew.h>

#include <iostream>

GLuint shader_creator(GLenum shaderType, const std::string &shaderpath);
GLuint program_creator(GLuint vertex_shader_id, GLuint fragment_shader_id, GLuint geometry_shader_id=0);

#endif //CENTAURI_SHADER_COMPILER_H
