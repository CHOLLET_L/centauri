#version 330 core

layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec2 vertexUV;

uniform mat4 VP;

out vec2 uv;

void main(){
    vec4 pos = VP * vec4(vertexPosition_modelspace, 1.0);
    gl_Position = pos.xyww;
    uv = vertexUV;
}