#version 330 core

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 colors_in;

uniform mat4 MVP;

out vec3 colors;

void main() {
    gl_Position = MVP * vec4(vertexPosition, 1);
    colors = colors_in;
}
