#version 330

layout (points) in;
layout (points, max_vertices = 1) out;

in vec2 uv;
out vec2 uv2;

void main() {
    gl_Position = gl_in[0].gl_Position;
    EmitVertex();
    EndPrimitive();

    uv2=uv;
}
