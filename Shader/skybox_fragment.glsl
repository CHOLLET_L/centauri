#version 330 core

out vec4 FragColor;

in vec3 vertexPosition_out;

uniform samplerCube skybox_cube;

void main()
{
    FragColor = textureCube(skybox_cube, vertexPosition_out);
//    FragColor = vec3(0.5, 0.5, 0.5);
//    FragColor = vec4(vertexPosition_out,1);
}