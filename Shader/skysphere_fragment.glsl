#version 330

in vec2 uv;

uniform sampler2D texture_sampler;

out vec4 FragColor;

void main() {
    FragColor = texture(texture_sampler, uv);
//    FragColor = vec4(1, 0, 0, 0);
}
