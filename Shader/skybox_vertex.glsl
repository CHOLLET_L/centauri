#version 330 core

layout (location = 0) in vec3 vertexPosition;

out vec3 vertexPosition_out;

uniform mat4 VP;

void main()
{
    vertexPosition_out = vertexPosition;
    vec4 pos = VP * vec4(vertexPosition, 1.0);
    gl_Position = pos.xyww;
}