#include <GL/glew.h>

#include <vector>
#include <fstream>

#include "../common/log.h"


std::string read_file(const std::string &filepath){
    std::ifstream myfile(filepath, std::ios::in);
    std::string content;
    if (myfile){
        std::string line;
        while (getline(myfile,line))
        {
            content += line + '\n';
        }
        myfile.close();
    }
    else{
        dprint("Unable to open Shader file", 2);
    }
    return content;
}

GLuint shader_creator(GLenum shaderType, const std::string &shaderpath){

    GLuint shader_id = glCreateShader(shaderType);

    std::string shader_source = read_file(shaderpath);

    GLint Result = GL_FALSE;
    int InfoLogLength;

    char const * shader_source_pointer = shader_source.c_str();
    glShaderSource(shader_id, 1, &shader_source_pointer , nullptr);
    glCompileShader(shader_id);

    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
        glGetShaderInfoLog(shader_id, InfoLogLength, nullptr, &VertexShaderErrorMessage[0]);
        dprint(&VertexShaderErrorMessage[0] , 2);
    }
//    else{
//        dprint("Shader: " + shaderpath + " Compiled", 0);
//    }


    return shader_id;
}

GLuint program_creator(GLuint vertex_shader_id, GLuint fragment_shader_id, GLuint geometry_shader_id=0){

    GLuint ProgramID = glCreateProgram();
    glAttachShader(ProgramID, vertex_shader_id);
    glAttachShader(ProgramID, fragment_shader_id);

    if (geometry_shader_id!=0){
        glAttachShader(ProgramID, geometry_shader_id);
    }

    glLinkProgram(ProgramID);


    GLint Result = GL_FALSE;
    int InfoLogLength;

    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> ProgramErrorMessage(InfoLogLength+1);
        glGetProgramInfoLog(ProgramID, InfoLogLength, nullptr, &ProgramErrorMessage[0]);
        dprint(&ProgramErrorMessage[0], 2);
    }
//    else {
//        dprint("Program: Linked", 0);
//    }


    glDetachShader(ProgramID, vertex_shader_id);
    glDetachShader(ProgramID, fragment_shader_id);

    glDeleteShader(vertex_shader_id);
    glDeleteShader(fragment_shader_id);


    return ProgramID;
}